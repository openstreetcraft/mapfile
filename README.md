# mapfile

Java library for reading/writing images within tile sets in 
[MBTiles format](https://github.com/mapbox/mbtiles-spec/blob/master/1.3/spec.md).

## Installation

See [GitLab Package Registry](https://gitlab.com/openstreetcraft/mapfile/-/packages/21839947).

## Usage

### Create meta information
To create a map file, some metadata must be defined in accordance with 
[the MBTiles specification](https://github.com/mapbox/mbtiles-spec/blob/master/1.3/spec.md#content)

```
MapInfo mapinfo = MapInfo.builder()
  .name("openstreetcraft biome map")
  .minzoom(1)
  .maxzoom(22)
  .bounds(MapInfo.Bounds.builder()
    .left(-180.0)
    .bottom(-85.0)
    .right(180.0)
    .top(85.0)
    .build())
  .center(MapInfo.Center.builder()
    .longitude(0.0)
    .latitude(0.0)
    .zoom(5)
    .build())
  .build();
```

### Write map file

```
import java.awt.image.BufferedImage;

// create map file
MapFile mapfile = MapFile.create("filename.map", mapinfo);

// create image
BufferedImage image = ...

// define tile coordinate
MapTile tile = MapTile.builder()
  .column(1)
  .row(2)
  .zoom(5)
  .build();

// save image at tile coordinate
mapfile.writeImage(tile, image);

mapfile.close();
```

### Read map file

```
// open map file for reading
MapFile mapfile = MapFile.open("filename.map");

// read meta information
MapInfo mapinfo = mapfile.getInfo();

// read image at tile coordinate
BufferedImage image = mapfile.readImage(tile)

mapfile.close();
```

### Finalize map file

Rebuilds the database file, repacking it into a minimal amount of disk space.
A finalized map file is read-only. Subsequent calls of write functions throw exceptions.

```
mapfile.finalize();
```

## License

[GNU LGPL Version 3](https://www.gnu.org/licenses/lgpl-3.0.html)
