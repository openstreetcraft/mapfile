package net.openstreetcraft.mapfile;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import net.openstreetcraft.mapfile.MapTile.MapTileBuilder;

class MapFileTest {

  MapInfo mapinfo;
  String filename;

  @BeforeEach
  void setUp(@TempDir Path tempdir) throws Exception {
    filename = tempdir.resolve("subdir/test.sqlite").toString();
    mapinfo = MapInfo.builder()
        .bounds(MapInfo.Bounds.builder().build())
        .center(MapInfo.Center.builder().build())
        .build();
  }
  
  @Test
  void readInfo() throws SQLException, IOException {
    try (MapFile mapfile = MapFile.create(filename, mapinfo)) {
      assertEquals(mapinfo, mapfile.getInfo());
    }
    try (MapFile mapfile = MapFile.open(filename)) {
      assertEquals(mapinfo, mapfile.getInfo());
    }
  }

  @Test
  void bytesNotFound() throws SQLException, IOException {
    MapTile tile = MapTile.builder().build();
    String expected = "MapTile(column=0, row=0, zoom=0) not found";
    try (MapFile mapfile = MapFile.create(filename, mapinfo)) {
      assertMessage(expected, assertThrows(IllegalArgumentException.class, () -> mapfile.readBytes(tile)));
    }
    try (MapFile mapfile = MapFile.open(filename)) {
      assertMessage(expected, assertThrows(IllegalArgumentException.class, () -> mapfile.readBytes(tile)));
    }
  }

  @Test
  void imageNotFound() throws SQLException, IOException {
    MapTile tile = MapTile.builder().build();
    String expected = "MapTile(column=0, row=0, zoom=0) not found";
    try (MapFile mapfile = MapFile.create(filename, mapinfo)) {
      assertMessage(expected, assertThrows(IllegalArgumentException.class, () -> mapfile.readImage(tile)));
    }
    try (MapFile mapfile = MapFile.open(filename)) {
      assertMessage(expected, assertThrows(IllegalArgumentException.class, () -> mapfile.readImage(tile)));
    }
  }

  private static void assertMessage(String expected, IllegalArgumentException exception) {
    assertEquals(expected, exception.getMessage());
  }

  @Test
  void readWriteBytes() throws SQLException, IOException {
    byte[] bytes = "foo".getBytes();
    MapTile tile = MapTile.builder().build();
    try (MapFile mapfile = MapFile.create(filename, mapinfo)) {
      mapfile.writeBytes(tile, bytes);
      assertArrayEquals(bytes, mapfile.readBytes(tile));
    }
    try (MapFile mapfile = MapFile.open(filename)) {
      assertArrayEquals(bytes, mapfile.readBytes(tile));
    }
  }

  @Test
  void readWriteImage() throws SQLException, IOException {
    BufferedImage expected = testImage();
    MapTileBuilder tile = MapTile.builder();
    try (MapFile mapfile = MapFile.create(filename, mapinfo)) {
      mapfile.writeImage(tile.column(1).build(), testImage());
      mapfile.writeImage(tile.column(2).build(), testImage());
      mapfile.writeImage(tile.column(3).build(), testImage());
      BufferedImage actual = mapfile.readImage(tile.build());
      assertImageEquals(expected, actual);
    }
    try (MapFile mapfile = MapFile.open(filename)) {
      BufferedImage actual = mapfile.readImage(tile.build());
      assertImageEquals(expected, actual);
    }
  }

  @Test
  void isClosed() throws SQLException, IOException {
    MapFile mapfile = MapFile.create(filename, mapinfo);
    assertFalse(mapfile.isClosed());
    mapfile.close();
    assertTrue(mapfile.isClosed());
  }

  @Test
  void isFinalized() throws SQLException, IOException {
    try (MapFile mapfile = MapFile.create(filename, mapinfo)) {
      assertFalse(mapfile.isFinalized());
      mapfile.finalize();
      assertTrue(mapfile.isFinalized());
    }
  }

  @Test
  @Disabled // dump() requires sqlite3 command line tool
  void save(@TempDir File tempdir) throws SQLException, IOException {
    String tempfile = tempdir.getAbsolutePath() + "/test.db";
    MapTileBuilder tile = MapTile.builder();
    try (MapFile mapfile = MapFile.create(filename, mapinfo)) {
      mapfile.writeBytes(tile.column(1).build(), "foo".getBytes());
      mapfile.writeBytes(tile.column(2).build(), "foo".getBytes());
      mapfile.writeBytes(tile.column(3).build(), "bar".getBytes());
      mapfile.save(tempfile);
    }
    try (MapFile mapfile = MapFile.open(tempfile)) {
      String expected = "PRAGMA foreign_keys=OFF;\n" + 
          "BEGIN TRANSACTION;\n" + 
          "CREATE TABLE metadata (name TEXT, value TEXT);\n" + 
          "INSERT INTO metadata VALUES('name','openstreetcraft');\n" + 
          "INSERT INTO metadata VALUES('format','png');\n" + 
          "INSERT INTO metadata VALUES('bounds','0, 0, 0, 0');\n" + 
          "INSERT INTO metadata VALUES('center','0, 0, 0');\n" + 
          "INSERT INTO metadata VALUES('minzoom','0');\n" + 
          "INSERT INTO metadata VALUES('maxzoom','0');\n" + 
          "CREATE TABLE tile_data(id INTEGER PRIMARY KEY AUTOINCREMENT,tile_data BLOB NOT NULL);\n" + 
          "INSERT INTO tile_data VALUES(1,X'666f6f');\n" + 
          "INSERT INTO tile_data VALUES(2,X'626172');\n" + 
          "CREATE TABLE tile_grid(zoom_level INTEGER NOT NULL,tile_column INTEGER NOT NULL,tile_row INTEGER NOT NULL,tile_data INTEGER NOT NULL,FOREIGN KEY(tile_data) REFERENCES tile_data(id));\n" + 
          "INSERT INTO tile_grid VALUES(0,1,0,1);\n" + 
          "INSERT INTO tile_grid VALUES(0,2,0,1);\n" + 
          "INSERT INTO tile_grid VALUES(0,3,0,2);\n" + 
          "DELETE FROM sqlite_sequence;\n" + 
          "INSERT INTO sqlite_sequence VALUES('tile_data',2);\n" + 
          "CREATE UNIQUE INDEX tile_grid_index ON tile_grid(zoom_level, tile_column, tile_row);\n" + 
          "CREATE VIEW tiles AS SELECT zoom_level, tile_column, tile_row, tile_data.tile_data FROM tile_grid, tile_data WHERE tile_grid.tile_data = tile_data.id;\n" + 
          "COMMIT;\n";
      assertEquals(expected, mapfile.dump());
    }
  }

  @Test
  @Disabled
  void performanceTest() throws SQLException, IOException {
    MapTileBuilder tile = MapTile.builder();
    try (MapFile mapfile = MapFile.create(filename, mapinfo)) {
      for (int n = 0; n < 1000; n++) {
        String text = UUID.randomUUID().toString();
        mapfile.writeBytes(tile.column(n).build(), text.getBytes());
      }
    }
  }

  private static void assertImageEquals(BufferedImage expected, BufferedImage actual) {
    assertEquals(expected.getWidth(), actual.getWidth());
    assertEquals(expected.getHeight(), actual.getHeight());
    for (int x = 0; x < actual.getWidth(); x++) {
      for (int y = 0; y < actual.getHeight(); y++) {
        assertEquals(expected.getRGB(x, y), actual.getRGB(x, y));
      }
    }
  }

  private BufferedImage testImage() {
    try (InputStream resource = getClass().getResourceAsStream("/random16x16.png")) {
      return ImageIO.read(resource);
    } catch (IOException exception) {
      throw new AssertionError();
    }    
  }
}
