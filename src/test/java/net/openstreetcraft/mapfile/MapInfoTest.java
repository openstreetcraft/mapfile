package net.openstreetcraft.mapfile;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MapInfoTest {

  @Test
  void formatBounds() {
    MapInfo.Bounds bounds = MapInfo.Bounds.builder()
        .left(1)
        .bottom(2.2)
        .right(3.33)
        .top(-0.4)
        .build();
    assertEquals("1, 2.2, 3.33, -0.4", bounds.toString());
  }

  @Test
  void parseBounds() {
    MapInfo.Bounds expected = MapInfo.Bounds.builder()
        .left(1)
        .bottom(2.2)
        .right(3.33)
        .top(-0.4)
        .build();
    MapInfo.Bounds actual = MapInfo.Bounds.parse("1, 2.2, 3.33, -0.4");
    assertEquals(expected, actual);
  }

  @Test
  void formatCenter() {
    MapInfo.Center center = MapInfo.Center.builder()
        .longitude(1.1)
        .latitude(-0.2)
        .zoom(3)
        .build();
    assertEquals("1.1, -0.2, 3", center.toString());
  }

  @Test
  void parseCenter() {
    MapInfo.Center expected = MapInfo.Center.builder()
        .longitude(1.1)
        .latitude(-0.2)
        .zoom(3)
        .build();
    MapInfo.Center actual = MapInfo.Center.parse("1.1, -0.2, 3");
    assertEquals(expected, actual);
  }

}
