package net.openstreetcraft.exception;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class CheckedExceptionTest {

  @Test
  void uncheckConsumer() {
    assertThrows(Exception.class, () -> CheckedException.uncheckConsumer(this::throwingConsumer).accept(null));
  }

  @Test
  void uncheckSupplier() {
    assertThrows(Exception.class, () -> CheckedException.uncheckSupplier(this::throwingSupplier).get());
  }

  @Test
  void uncheckFunction() {
    assertThrows(Exception.class, () -> CheckedException.uncheckFunction(this::throwingFunction).apply(null));
  }

  void throwingConsumer(Object s) throws Exception {
    throw new Exception();
  };
  
  Object throwingSupplier() throws Exception {
    throw new Exception();
  };

  Object throwingFunction(Object s) throws Exception {
    throw new Exception();
  };
}
