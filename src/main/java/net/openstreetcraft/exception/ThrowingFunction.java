package net.openstreetcraft.exception;

import java.util.function.Function;

@FunctionalInterface
public interface ThrowingFunction<T, R, E extends Throwable> {
  R apply(T t) throws E;
}
