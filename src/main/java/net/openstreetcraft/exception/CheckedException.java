package net.openstreetcraft.exception;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public final class CheckedException {

  private CheckedException() {}
  
  public static <T, E extends Throwable> Consumer<T> uncheckConsumer(ThrowingConsumer<T, E> consumer) {
    return t -> {
      try {
        consumer.accept(t);
      } catch (Throwable e) {
        throw rethrow(e);
      }
    };
  }

  public static <T, E extends Throwable> Supplier<T> uncheckSupplier(ThrowingSupplier<T, E> supplier) {
    return () -> { 
      try {
        return supplier.get();
      } catch (Throwable e) {
        throw rethrow(e);
      }
    };
  }

  public static <T, R, E extends Throwable> Function<T, R> uncheckFunction(ThrowingFunction<T, R, E> function) {
    return t -> {
      try {
        return function.apply(t);
      } catch (Throwable e) {
        throw rethrow(e);
      }
    };
  }

  @SuppressWarnings("unchecked")
  private static <T extends Throwable> RuntimeException rethrow(Throwable throwable) throws T {
    throw (T) throwable;
  }
}
