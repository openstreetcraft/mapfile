package net.openstreetcraft.mapfile;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class MapTile {
  int column;
  int row;
  int zoom;
}
