package net.openstreetcraft.mapfile;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Base64;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.imageio.ImageIO;

import lombok.Getter;
import net.openstreetcraft.exception.CheckedException;

public class MapFile implements AutoCloseable {

  private final Connection connection;
  private final Transaction transaction;
  private final Map<String, PreparedStatement> preparedStatements;

  @Getter(lazy = true)
  private final MapInfo info = info();

  private class Transaction {
    final AtomicBoolean open = new AtomicBoolean(false);
    
    void open() throws SQLException {
      if (!open.getAndSet(true)) {
        execute("BEGIN");
      }
    }
    
    void close() throws SQLException {
      if (open.getAndSet(false)) {
        execute("END");
      }
    }
    
    void execute(String prefix) throws SQLException {
      try (Statement statement = connection.createStatement()) {
        statement.executeUpdate(prefix + " TRANSACTION");
      }
    }
  }

  private MapFile(Connection connection) throws SQLException {
    this.connection = connection;
    this.transaction = new Transaction();
    this.preparedStatements = new ConcurrentHashMap<>();
  }
  
  public static MapFile create(String filename, MapInfo info) throws SQLException, IOException {
    Files.createDirectories(Paths.get(filename).getParent());
    try (Connection connection = DriverManager.getConnection(url(filename));
        Statement statement = connection.createStatement()) {
      create(statement, info);
    }
    return open(filename);
  }

  private static String url(String filename) {
    return "jdbc:sqlite:" + filename;
  }

  private static void create(Statement statement, MapInfo info) throws SQLException {
    statement.executeUpdate("PRAGMA synchronous = OFF");
    statement.executeUpdate("PRAGMA journal_mode = OFF");

    statement.executeUpdate("BEGIN TRANSACTION");
    statement.executeUpdate("CREATE TABLE metadata (name TEXT, value TEXT)");

    insertMetadata(statement, "name", info.getName());
    insertMetadata(statement, "format", info.getFormat());
    insertMetadata(statement, "bounds", info.getBounds());
    insertMetadata(statement, "center", info.getCenter());
    insertMetadata(statement, "minzoom", info.getMinzoom());
    insertMetadata(statement, "maxzoom", info.getMaxzoom());

    statement.executeUpdate("CREATE TABLE tile_data("
        + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
        + "hash TEXT NOT NULL,"
        + "tile_data BLOB NOT NULL)");

    statement.executeUpdate("CREATE UNIQUE INDEX tile_data_index"
        + " ON tile_data(hash)");

    statement.executeUpdate("CREATE TABLE tile_grid("
        + "zoom_level INTEGER NOT NULL,"
        + "tile_column INTEGER NOT NULL,"
        + "tile_row INTEGER NOT NULL,"
        + "tile_data INTEGER NOT NULL,"
        + "FOREIGN KEY(tile_data) REFERENCES tile_data(id))");

    statement.executeUpdate("CREATE UNIQUE INDEX tile_grid_index"
        + " ON tile_grid(zoom_level, tile_column, tile_row)");

    statement.executeUpdate("CREATE VIEW tiles AS"
        + " SELECT zoom_level, tile_column, tile_row, tile_data.tile_data"
        + " FROM tile_grid, tile_data"
        + " WHERE tile_grid.tile_data = tile_data.id");

    statement.executeUpdate("COMMIT");
  }

  private static <T> void insertMetadata(Statement statement, String name, T value) throws SQLException {
    Objects.requireNonNull(value, name);
    statement.executeUpdate(String.format("INSERT INTO metadata VALUES('%s', '%s')", name, value));
  }

  private static void insertMetadata(Statement statement, String name, int value) throws SQLException {
    statement.executeUpdate(String.format("INSERT INTO metadata VALUES('%s', %d)", name, value));
  }

  public static MapFile open(String filename) throws SQLException {
    return new MapFile(DriverManager.getConnection(url(filename)));
  }

  public synchronized void save(String filename) throws SQLException {
    transaction.close();
    try (Statement statement = connection.createStatement()) {
      statement.executeUpdate("BACKUP TO " + filename);
    } finally {
      try (MapFile mapFile = open(filename)) {
        mapFile.finalize();
      }
    }
  }

  @Override
  public synchronized void close() throws SQLException {
    transaction.close();
    preparedStatements.clear();
    connection.close();
  }
  
  public boolean isClosed() throws SQLException {
    return connection.isClosed();
  }
  
  public synchronized void finalize() throws SQLException {
    transaction.close();
    try (Statement statement = connection.createStatement()) {
      statement.executeUpdate("DROP INDEX tile_data_index");
      statement.executeUpdate("ALTER TABLE tile_data DROP COLUMN hash");
      statement.executeUpdate("VACUUM");
    }
  }

  public boolean isFinalized() throws SQLException {
    try (Statement statement = connection.createStatement()) {
      String sql = "SELECT * FROM sqlite_master WHERE type = 'index' AND name = 'tile_data_index'";
      ResultSet result = statement.executeQuery(sql);
      return !result.isBeforeFirst();
    }
  }
  
  public void writeImage(MapTile tile, BufferedImage image) throws SQLException, IOException {
    try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
      ImageIO.write(image, getInfo().getFormat(), output);
      writeBytes(tile, output.toByteArray());
    }
  }

  public void writeBytes(MapTile tile, byte[] bytes) throws SQLException {
    transaction.open();
    insertTileGrid(tile, hash(bytes));
  }

  private synchronized int hash(byte[] bytes) throws SQLException {
    String hash = Base64.getEncoder().encodeToString(bytes);
    int id = selectTileData(hash);
    if (id != 0) {
      return id;
    }
    return insertTileData(hash, bytes);
  }

  private int selectTileData(String hash) throws SQLException {
    String sql = "SELECT id FROM tile_data WHERE hash = ?";
    PreparedStatement statement = prepareStatement(sql);
    statement.setString(1, hash);
    try (ResultSet result = statement.executeQuery()) {
      return result.getInt(1);
    }
  }

  private int insertTileData(String hash, byte[] bytes) throws SQLException {
    String sql = "INSERT INTO tile_data(hash, tile_data) VALUES(?, ?)";
    PreparedStatement statement = prepareStatement(sql);
    statement.setString(1, hash);
    statement.setBytes(2, bytes);
    statement.executeUpdate();
    return selectTileData(hash);
  }

  private void insertTileGrid(MapTile tile, int id) throws SQLException {
    String sql = "INSERT INTO tile_grid(zoom_level, tile_column, tile_row, tile_data) VALUES(?, ?, ?, ?)";
    PreparedStatement statement = prepareStatement(sql);
    statement.setInt(1, tile.getZoom());
    statement.setInt(2, tile.getColumn());
    statement.setInt(3, tile.getRow());
    statement.setInt(4, id);
    statement.execute();
  }

  public BufferedImage readImage(MapTile tile) throws SQLException, IOException, IllegalArgumentException {
    return read(tile, ImageIO::read);
  }

  public byte[] readBytes(MapTile tile) throws SQLException, IOException, IllegalArgumentException {
    return read(tile, MapFile::readAllBytes);
  }

  @FunctionalInterface
  private interface CheckedFunction<T, R> {
    R apply(T t) throws IOException;
  }

  private <T> T read(MapTile tile, CheckedFunction<InputStream, T> reader)
      throws SQLException, IOException, IllegalArgumentException {
    String sql = "SELECT tile_data FROM tiles WHERE zoom_level = ? AND tile_column = ? AND tile_row = ?";
    PreparedStatement statement = prepareStatement(sql);

    statement.setInt(1, tile.getZoom());
    statement.setInt(2, tile.getColumn());
    statement.setInt(3, tile.getRow());

    try (ResultSet result = statement.executeQuery()) {
      if (!result.isBeforeFirst()) {
        throw new IllegalArgumentException(tile + " not found");
      }
      try (InputStream input = result.getBinaryStream(1)) {
        return reader.apply(input);
      }
    }
  }
  
  private PreparedStatement prepareStatement(String sql) throws SQLException {
    return preparedStatements.computeIfAbsent(sql, CheckedException.uncheckFunction(connection::prepareStatement));
  }

  private static byte[] readAllBytes(InputStream input) throws IOException {
    int size = 0x1000;
    byte[] buffer = new byte[size];
    int length;

    try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
      if (input != null) {
        while ((length = input.read(buffer, 0, size)) != -1) {
          output.write(buffer, 0, length);
        }
      }
      return output.toByteArray();
    }
  }

  private MapInfo info() {
    MapInfo.MapInfoBuilder info = MapInfo.builder();
    try (Statement statement = connection.createStatement()) {
      String sql = "SELECT name, value FROM metadata";
      try (ResultSet result = statement.executeQuery(sql)) {
        while (result.next()) {
          String name = result.getString(1);
          String value = result.getString(2);
          switch (name) {
            case "name":
              info.name(value);
              break;
            case "format":
              info.format(value);
              break;
            case "bounds":
              info.bounds(MapInfo.Bounds.parse(value));
              break;
            case "center":
              info.center(MapInfo.Center.parse(value));
              break;
            case "minzoom":
              info.minzoom(Integer.parseInt(value));
              break;
            case "maxzoom":
              info.maxzoom(Integer.parseInt(value));
              break;
          }
        }
      }
    } catch (SQLException exception) {
      throw new IllegalStateException(exception);
    }
    return info.build();
  }
  
  public String dump() throws IOException, SQLException {
    Path path = Files.createTempFile("mapfile", ".db");
    try (Statement statement = connection.createStatement()) {
      String sql = "BACKUP TO " + path;
      statement.executeUpdate(sql);
    }
    Process process = new ProcessBuilder("sqlite3", path.toString(), ".dump").start();
    byte[] bytes = readAllBytes(process.getInputStream());
    Files.deleteIfExists(path);
    return new String(bytes);
  }

}
