package net.openstreetcraft.mapfile;

import java.text.NumberFormat;
import java.util.Locale;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class MapInfo {

  /**
   * The human-readable name of the tileset.
   */
  @Builder.Default
  String name = "openstreetcraft";
  
  /**
   * The file format of the tile data: pbf, jpg, png, webp, or an IETF media type for other formats.
   */
  @Builder.Default
  String format = "png";
  
  /**
   * The maximum extent of the rendered map area. Bounds must define an area covered by all zoom
   * levels. The bounds are represented as WGS 84 latitude and longitude values, in the OpenLayers
   * Bounds format ("left, bottom, right, top").
   */
  Bounds bounds;
  
  /**
   * The longitude, latitude and zoom level of the default view of the map.
   */
  Center center;
  
  /**
   * The lowest zoom level for which the tileset provides data.
   */
  int minzoom;
  
  /**
   * The highest zoom level for which the tileset provides data.
   */
  int maxzoom;

  @Value
  @Builder
  public static class Bounds {
    double left, bottom, right, top;
    
    public String toString() {
      NumberFormat formatter = NumberFormat.getInstance(Locale.US);
      return String.format("%s, %s, %s, %s",
          formatter.format(left),
          formatter.format(bottom),
          formatter.format(right),
          formatter.format(top));
    }
    
    public static Bounds parse(String text) {
      String[] parts = text.split(", ");
      return builder()
          .left(Double.parseDouble(parts[0]))
          .bottom(Double.parseDouble(parts[1]))
          .right(Double.parseDouble(parts[2]))
          .top(Double.parseDouble(parts[3]))
          .build();
    }
  }

  @Value
  @Builder
  public static class Center {
    double longitude, latitude;
    int zoom;
    
    public String toString() {
      NumberFormat formatter = NumberFormat.getInstance(Locale.US);
      return String.format("%s, %s, %d",
          formatter.format(longitude),
          formatter.format(latitude),
          zoom);
    }

    public static Center parse(String text) {
      String[] parts = text.split(", ");
      return builder()
          .longitude(Double.parseDouble(parts[0]))
          .latitude(Double.parseDouble(parts[1]))
          .zoom(Integer.parseInt(parts[2])).build();
    }
  }
}
